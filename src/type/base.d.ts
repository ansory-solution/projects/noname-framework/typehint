// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
// This File is From Ansory Solution
// Licensed under GNU GENERAL PUBLIC LICENSE Version 3
// File: base.d.ts (ansory-solution/projects/noname-framework/typehint/type/base.d.ts)
// Content: util type to use.
// Copyright (c) 2023 ansolve All rights reserved
// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

/**
 * 空的对象，用于表示`{}`
 */
export type EmptyObject = Record<string, never>;
