// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
// This File is From Ansory Solution
// Licensed under GNU GENERAL PUBLIC LICENSE Version 3
// File: skill.d.ts (ansory-solution/projects/noname-framework/typehint/type/skill.d.ts)
// Content: noname skill type.
// Copyright (c) 2023 ansolve All rights reserved
// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

/**
 * 可以当作时机给予`Skill#trigger`和`Player#when`的值
 *
 * > 命名待定
 */
export type OccasionGiven = string | string[];
