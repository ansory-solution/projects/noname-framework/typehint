// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
// This File is From Ansory Solution
// Licensed under GNU GENERAL PUBLIC LICENSE Version 3
// File: element.d.ts (ansory-solution/projects/noname-framework/typehint/src/lib/element.d.ts)
// Content: `lib.element` namespace.
// Copyright (c) 2023 ansolve All rights reserved
// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

import { Card } from "./element/card";
import { VCard } from "./element/vcard";

export { Card, VCard };

export const card: Card;
