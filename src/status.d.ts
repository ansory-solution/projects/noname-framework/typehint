// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
// This File is From Ansory Solution
// Licensed under GNU GENERAL PUBLIC LICENSE Version 3
// File: status.d.ts (ansory-solution/projects/noname-framework/typehint/src/status.d.ts)
// Content: noname varible _status.
// Copyright (c) 2023 ansolve All rights reserved
// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

import { Card } from "./lib/element/card";
import { GameEvent } from "./lib/element/event";
import { CardIdentity } from "./type/card";

export let paused: boolean;

export let paused2: boolean;

export let paused3: boolean;

export let over: boolean;

export let clicked: boolean;

export let auto: boolean;

export let event: GameEvent;

/**
 * @todo
 */
export let ai: {
	customAttitude: (() => number)[];
	[key: string]: unknown;
};

export let lastdragchange: unknown[];

export let skillaudio: string[];

/**
 * @todo
 */
export let dieClose: unknown[];

export let dragline: unknown[];

/**
 * @todo
 */
export let dying: unknown[];

export let globalHistory: {
	cardMove: GameEvent[];
	custom: GameEvent[];
	useCard: GameEvent<"useCard">[];
	changeHp: GameEvent<"changeHp">[];
	everything: GameEvent[];
}[];

export let cardtag: {
	yingbian_zhuzhan: CardIdentity[];
	yingbian_kongchao: CardIdentity[];
	yingbian_fujia: CardIdentity[];
	yingbian_canqu: CardIdentity[];
	yingbian_force: CardIdentity[];
};

export let renku: Card[];

export let prehidden_skills: string[];

export let postReconnect: object;
